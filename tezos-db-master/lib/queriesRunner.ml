let pool = Config.getPool

type error =
  | Database_error of string

(* Helper method to map Caqti errors to our own error type. 
   val or_error : ('a, [> Caqti_error.t ]) result Lwt.t -> ('a, error) result Lwt.t *)
let or_error m =
  match%lwt m with
  | Ok a -> Ok a |> Lwt.return
  | Error e -> Error (Database_error (Caqti_error.show e)) |> Lwt.return

let execQuery query =
  Caqti_request.exec
  Caqti_type.unit
  query

let queryRunner query = 
  let migrate' (module C : Caqti_lwt.CONNECTION)  =
    let migrateQuery = execQuery query
    in
    C.exec migrateQuery ()
  in
  let m = Caqti_lwt.Pool.use migrate' pool |> or_error in
  Lwt_main.run (match%lwt m with
  | Ok () -> print_endline "Query exec done." |> Lwt.return
  | Error (Database_error msg) -> print_endline msg |> Lwt.return)

let arrayQuery ar = 
  for i = 0 to Array.length ar - 1 do
    queryRunner (Array.get ar i)
  done

let run queries =
  arrayQuery queries