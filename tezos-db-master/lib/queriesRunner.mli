type error =
  | Database_error of string

(* Migrations-related helper functions. Run array of SQL queries.*)
val run : string array -> unit