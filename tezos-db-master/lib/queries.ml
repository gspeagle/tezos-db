module type DBC = Caqti_lwt.CONNECTION
let pool = Config.getPool
type todo = {
  id: int;
  content: string;
}

type error =
  | Database_error of string

(* Helper method to map Caqti errors to our own error type. 
   val or_error : ('a, [> Caqti_error.t ]) result Lwt.t -> ('a, error) result Lwt.t *)
let or_error m =
  match%lwt m with
  | Ok a -> Ok a |> Lwt.return
  | Error e -> Error (Database_error (Caqti_error.show e)) |> Lwt.return

let execution expr = Caqti_lwt.Pool.use expr pool |> or_error

let unitResultErrorHandler expr queryName = 
  Lwt_main.run (
    match%lwt (execution expr) with
      | Ok () -> print_endline (queryName ^ " done.") |> Lwt.return
      | Error (Database_error msg) -> print_endline (queryName ^ " error: " ^ msg) |> Lwt.return
  )

let selectQuery tableType tableName =
  Caqti_request.collect
    Caqti_type.unit 
    tableType
    ("SELECT * FROM " ^ tableName) 
  
let select tableType tableName dataAccamulator =
  let selectWrapper (module DB : DBC) =
    DB.fold (selectQuery tableType tableName) dataAccamulator () [] in
  Lwt_main.run (
    match%lwt (execution selectWrapper) with
      | Ok (result) -> print_endline "Select done."; result |> Lwt.return
      | Error (Database_error msg) -> print_endline ("Select error: " ^ msg); [] |> Lwt.return
  ) 
   
let insertQuery tableType query =
  Caqti_request.exec
  tableType
  query

let insert insertType query content =
  let insertWrapper content (module DB : DBC) =
    DB.exec (insertQuery insertType query) content
  in
  unitResultErrorHandler (insertWrapper content) "Insert"

let deleteQuery tableName =
  Caqti_request.exec
    Caqti_type.int
    ("DELETE FROM " ^ tableName ^ " WHERE id = ?")

let delete tableName id =
  let deleteWrapper tableName id (module DB : DBC) =
    DB.exec (deleteQuery tableName) id
  in
  unitResultErrorHandler (deleteWrapper tableName id) "Delete"

let truncateQuery tableName =
  let query = "TRUNCATE TABLE " ^ tableName in
  Caqti_request.exec
    Caqti_type.unit
    query

let truncateTable tableName =
  let truncateTableWrapper (module DB : DBC) =
    DB.exec (truncateQuery tableName) ()
  in
  unitResultErrorHandler truncateTableWrapper "Truncate"