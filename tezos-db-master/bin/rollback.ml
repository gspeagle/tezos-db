open Lib

let dropTableScripts = [|
  MigrationScripts.dropBlockchainInformationTable;
  MigrationScripts.dropCaseFileTable;
  MigrationScripts.dropEvifancePiecesTable;
  MigrationScripts.dropLocationTable;
  MigrationScripts.dropPersonsTable;
|]

let () = QueriesRunner.run dropTableScripts
