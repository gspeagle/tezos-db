let dropPersonsTable = "DROP TABLE IF EXISTS public.PERSONS;"
let dropLocationTable = "DROP TABLE IF EXISTS public.LOCATION;"
let dropCaseFileTable = "DROP TABLE IF EXISTS public.CASE_FILE;"
let dropEvifancePiecesTable = "DROP TABLE IF EXISTS public.EVIDENC_PIECES;"
let dropBlockchainInformationTable = "DROP TABLE IF EXISTS public.BLOCKCHAIN_INFORMATION;"

let createPersonsTable = 
"CREATE TABLE public.PERSONS
(
  ID SERIAL PRIMARY KEY,
  First_Name character varying,
  Surname character varying,
  Department character varying,
  crypto_id  character varying,
  active boolean,
  logins timestamp,
  case_files character varying
)
WITH (
  OIDS=FALSE
);"

let createLocationTable =
"CREATE TABLE public.LOCATION
(
  ID SERIAL PRIMARY KEY,
  Location_name character varying,
  location_crypto_id character varying,
  location_address character varying,
  Date_in timestamp,
  Date_out timestamp,
  Date_purged timestamp
)
WITH (
  OIDS=FALSE
);"

let createCaseFileTable = 
"CREATE TABLE public.CASE_FILE
  (
    ID SERIAL PRIMARY KEY,
    Name character varying,
    Case_hash character varying,
    case_address character varying,
    first_officer character varying,
    first_tech character varying,
    case_origin timestamp,
    law_deparmtent character varying,
    dep_hash_id character varying,
    Evdience_tokens character varying,
    token_name character varying,
    count_evidence_pieces integer CHECK(count_evidence_pieces > 0),
    transactions integer CHECK(transactions > 0)
  )
  WITH (
    OIDS=FALSE
  );"

let createEvidencPiecesTable = 
"CREATE TABLE public.EVIDENC_PIECES
  (
    ID SERIAL PRIMARY KEY,
    Token_contract character varying,
    evidence_piece character varying,
    description character varying,
    unique_cyrpto_id character varying,
    attached_token boolean
  )
  WITH (
    OIDS=FALSE
  );"
    
let createBlockchainInformationTable = 
"CREATE TABLE public.BLOCKCHAIN_INFORMATION
  (
    ID SERIAL PRIMARY KEY,
    case_name character varying,
    case_hash character varying,
    contract_address character varying,
    token_name character varying,
    token_symbol character varying,
    token_creation timestamp,
    token_purge timestamp,
    token_balance integer CHECK(token_balance > 0)
  )
  WITH (
    OIDS=FALSE
  );"