const {network} = require('../config');
const {eztz} = require('./main');
eztz.node.setProvider(`https://${network}.tezrpc.me`);

module.exports = {
    eztz
}