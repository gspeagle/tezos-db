Instructions for Chrome Box(Asus CN60 in this case) Tezos node build

First, you need to remove the Write Protect screw. Remove the six screws holding on the bottom plate of the box and remove the plate. You should see the Write Protect screw as in the photos. Other models may vary where this screw is placed, but it is usually identifiable by a split metal contact underneath the screw.

![wpscrew3](uploads/2e0c595cbbe379b6d106fe5692899056/wpscrew3.jpg)
![wpscrew4](uploads/588acf3ea23b28ee84e620dd44b95522/wpscrew4.jpg)

Next you need to put the Chrome OS into Developer Mode. These instructions are also found on the chromium.org website. First you need to power off your Chrome Box.
There is a hole above the Kensington lock on the box. Insert a paperclip to push the Recovery button inside this hole and power on the box. Release the Recovery button after a second and the Recovery screen should appear.

At the Recovery screen hit CTRL+D. You will be asked to hit the recovery button again to confirm you want to enter Developer Mode. After Reboot you will have a Developer Mode warning screen which you can skip with CTRL+D. Once booted into Developer Mode, you can begin Mr.Chromebox's instructions for finding a root capable shell.
Open a Chrome browser and press CTRL+ALT+T, type shell and press enter. You now have a root capable shell terminal.

Now you can start the Firmware Update process from Mr. Chromebox.
At the shell, run the Mr. Chromebox Firmware Utility Script. 

```cd; curl -LO https://mrchromebox.tech/firmware-util.sh && sudo bash firmware-util.sh```

You are going to completely remove Chrome OS and install a standalone Linux system, so type 3 to choose FULL ROM FIRMWARE and press Enter. The script should automatically disable Write Protect, but if it doesn't you can run this command in the Chrome shell:

```flashrom --wp-disable```

The script will run through it's processes and once it is finished you should have a full UEFI firmware update.

Now you will need to make a bootable USB flashdrive with a Debian 9 image on it. With your bootable USB inserted, boot up the Chrome Box. If your machine doesn't automatically boot into the USB, but instead boots to an EFI shell, you can type exit to get into the Main Menu. From the Main Menu you can go to Boot Manager and choose which device to boot from. Choose the USB and follow the Debian instructions for a normal install.

Once your Debian install has completed, remove your installation media and reboot. You will boot directly to the EFI shell, and will need to type exit and hit Enter. From the Main Menu you will need to choose Boot Maintenance Manager. Then choose Boot From File and choose the first file option.Then choose EFI, debian, and grubx64.efi. This will get you into a recognizable GRUB bootloader and you can boot into your Debian system.
Now you can login to your Debian system and follow the Tezos Developer Documentation to set up your node.
